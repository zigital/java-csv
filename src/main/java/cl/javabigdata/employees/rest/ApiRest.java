package cl.javabigdata.employees.rest;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import cl.javabigdata.employees.config.ConfigProperties;
import cl.javabigdata.employees.model.Employe;

@RestController
@RequestMapping( path= "/rest/employe/v1" )
public class ApiRest {
	
	private final CloseableHttpClient httpClient = HttpClients.createDefault();
	
	@Autowired
	ConfigProperties configProp;

	
	@RequestMapping( path= "/getEmployees" , method = RequestMethod.GET)
	@ResponseBody
	public void getEmployees( @RequestParam(name = "edad") Integer edad ) {	
		ApiRest api = new ApiRest();
		try {
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			String rutaServer = configProp.getConfigValue("ruta.server");
			String nameFile = configProp.getConfigValue("name.file");
			
			
			System.out.println( timeStamp);
			
			String datosJson = api.getData();
			StringBuffer sJson = api.json2Csv( datosJson, edad );
			
			ChannelSftp channelSftp = setupJsch();
		    channelSftp.connect();
		  
		    Path path = Files.createTempFile( nameFile + timeStamp, ".csv");
	        File file = path.toFile();
	        
	        Files.write( path, sJson.toString().getBytes(StandardCharsets.UTF_8) );
	        file.deleteOnExit();
		    channelSftp.put( file.getAbsolutePath(), rutaServer + nameFile + timeStamp + ".csv");
		  
		    channelSftp.exit();
			
		    
		} catch (Exception e) {
			
		}
		
	}
	
	private String getData() throws Exception {

	    try {
			
	    	HttpGet request = new HttpGet( "http://dummy.restapiexample.com/api/v1/employees" );
	    	
	    	CloseableHttpResponse response = httpClient.execute(request);

	        // Get HttpResponse Status
	        System.out.println(response.getStatusLine().toString());

	        HttpEntity entity = response.getEntity();
	        Header headers = entity.getContentType();
	        System.out.println(headers);

	        if (entity != null) {
	            String result = EntityUtils.toString(entity);
	            
	            return result;
	            
	        }
	        return "";
	    } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }  
	    
	}
	
	private StringBuffer json2Csv( String input, Integer edad )  throws JSONException {
		
        try {
            JSONObject obj = new JSONObject( input );
            JSONArray arr= obj.getJSONArray("data");
          
            List<JSONObject> list = StreamSupport.stream( arr.spliterator(), false )
                    .map(val -> (JSONObject) val)
                    .filter(val -> val.getInt("employee_age") < edad)
                    .collect(Collectors.toList());

            Gson gson = new Gson();
            
            Employe[] empleadoArray = gson.fromJson(list.toString(), Employe[].class);
            StringBuffer salida = new StringBuffer(); 
            salida.append(  "profile_image, employee_name, employee_salary, id, employee_age\n");
            for( Employe e : empleadoArray ) {
            	salida.append(  e.getProfile_image() 
            					+ "," +  e.getEmployee_name()
            					+ "," + e.getEmployee_salary() 
            					+ "," + e.getId() 
            					+ "," + e.getEmployee_age() + "\n" );
            }
        
            return salida;
            
        } catch (JSONException e) {
            e.printStackTrace();
            throw new JSONException( "Error json2Csv ", e );
        }        
        
	}
	
	private ChannelSftp setupJsch() throws JSchException {
		String userServer = configProp.getConfigValue("user.server");
		String ipServer = configProp.getConfigValue("ip.server");
		String rutaServer = configProp.getConfigValue("ruta.server");
		String claveServer = configProp.getConfigValue("clave.server");
		
	    JSch jsch = new JSch();
	    jsch.setKnownHosts( rutaServer );
	    Session jschSession = jsch.getSession( userServer, ipServer);
	    jschSession.setPassword( claveServer );
	    
	    java.util.Properties config = new java.util.Properties(); 
	    config.put("StrictHostKeyChecking", "no");
	    jschSession.setConfig(config);
	    
	    jschSession.connect();
	    return (ChannelSftp) jschSession.openChannel("sftp");
	}
	

}

package cl.javabigdata.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaBigDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaBigDataApplication.class, args);
	}

}
